#!/bin/bash

export LC_ALL=C

pkgver=$(grep ^pkgver= PKGBUILD | cut -d'=' -f2)
_gnu=$(grep ^_gnu= PKGBUILD | cut -d'=' -f2)

git pull

makepkg -oLC

tar -C src/gnuzilla/output -cavf icecat-tarball-archlinux.tar.xz icecat-${pkgver}

echo "Package created: icecat-tarball-archlinux.tar.xz"
